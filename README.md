![Build Status](https://gitlab.com/pages/mkdocs/badges/master/pipeline.svg)


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.


## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
1. Install Docusaurus:

   ```sh
   cd website
   yarn install
   ```
  Your site can be accessed under http://localhost:3000.

1. Add content.
1. Generate the website (optional):

   ```sh
   yarn build
   ```

   The build directory is created based on the value set for `projectName` in
   `website/siteConfig.js`. If you didn't change this value, the website will
   be built under `website/build/docusaurus/`.

Read more at the [Docusaurus documentation](https://docusaurus.io).
