---
id: dependencies_installation
title: Dependencies Installation
sidebar_label: Dependencies Installation
---


<h3> REACT Dependencies </h3>

* Download and install [NodeJS](https://nodejs.org/en/download/)
* Please make sure you get an LTS version, appropriate to your OS
* Install create-react-app module from NPM which will be used to run this project


```
npm i create-react-app
```

<h3> SpringBoot Dependencies </h3>

Download and install below softwars:
* [Java 8](https://www.oracle.com/in/java/technologies/javase/javase-jdk8-downloads.html)
* [Gradle](https://gradle.org/install/)
* [Postgres](https://www.postgresql.org/download/)