---
id: admin_user_guide 
title: Admin User Guide
sidebar_label: Admin User Guide
---

This section will provide a quick overview on how to use the LMS admin dashboard. Topics covered:

- Adding new categories
- Editing new categories
- Adding new courses
- Editing new courses


<h3> Adding New Categories </h3>

1. Access your Admin panel (http://localhost:3000)
2. Click on LMS> Categories from the header above
    - This will open a view of all existing categories added thus far
3. Click the "Add" button on the top right hand corner<br>
![Add](/lms/img/admin_user_guide/add.png)
4. Under "Name" - type the new category name
5. Under "Description" - type the description you'd like users to see on the home page where category tiles are shown
6. Under "Colour" - type in the hex code for the colour you'd like the category tile on the home page to be
7. Finally, upload an image icon (png files preferred) that you'd like users to see be associated with this new category <br>
![Add](/lms/img/admin_user_guide/drag.png)
8. Click "Save" - this will add your new category to the category view
9. Finally, check all the newly added details are correct and click "Publish" to publish your changes


<h3> Editing new categories </h3>

1. To edit an existing category, first open the category view on the admin dashboard and find the relevant category
2. Click "Unpublish"
3. Click "Edit"
4. Make the necessary changes
5. Click "Save"
6. Click "Publish"

<h3> Adding New Courses </h3>

1. Access your Admin panel ([http://localhost:3000](http://localhost:3000/))
2. Click on LMS> Courses from the header above
    - This will open a view of all existing courses added thus far
3. Click the "Add" button on the top right hand corner

    ![Add](/lms/img/admin_user_guide/add.png)

4. Start by defining the Name, Course Overview, Category that the course belongs to and the header image for the course (we recommend 850x300px)
5. Next, begin by adding chapters
6. Write out the content for each chapter using the built in HTML editor
    - Format the text using the available formatting
         ![Add](/lms/img/admin_user_guide/content.png)

    - Images, videos, links and quotes can be added using the editor
    - Enter the chapter name here

        ![Add](/lms/img/admin_user_guide/chapter_name.png)

    - Once done with the first chapter, click "Add Chapter" to begin with the second
    - Chapters can be reordered using the "Move" buttons as below

        ![Add](/lms/img/admin_user_guide/move.png)

7. Once all the chapters have been added, click "Save"
8. Next, find the course in the course table and click publish to go live!

 ![Add](/lms/img/admin_user_guide/course_table.png)

<h3> Editing new courses </h3>


1. To edit an existing course, first open the Course View on the admin dashboard and find the relevant course
2. Click "Unpublish"
3. Click "Edit"
4. Make the necessary changes to the title, overview or relevant chapters
5. Click "Save"
6. Click "Publish"