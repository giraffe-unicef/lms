---
id: project_overview
title: Project Overview
sidebar_label: Project Overview
---

Project is divided in two parts 

1. Admin Site
2. End User Site

<h3>Admin panel</h3>

An administrator can create categories & courses, and publish them onto their LMS instance using CRUDs (Create, Read, Update and Delete).

<h6><b>Category CRUD</b></h6>

* **Fields** : Name, Description, Color(Used as a background for card where category is shown), Logo
* A published category cannot be edited
* Categories can be created & deleted or published & unpublished from this CRUD

<h6><b> Courses CRUD </b></h6>

* **Fields** : Name, Description, Category, Logo, Chapters (Array)
* Each chapter consists of name and chapter content, Also chapters sequence can also be decided changed there.
* Courses can be published/unpublished from Table


<h3>End User Site</h3>

The end user site is structured into 5 discreet sections:

<h6><b> Home Page </b></h6>

Used to house site description and context. It includes a lane for users to browse course categories.

<h6><b> Browse Courses </b></h6>

A view to allow users to browse a list of all available courses on the LMS. Users can sort/ filter courses by Category as well as Date Uploaded.

This view shows high-level course details such as:
* Course Overview
* <span># of likes</span>
* Estimated read time
* Upload date

Clicking on the enrol CTA will take the users to the "Course Detail" page

<h6><b> Course Detail </b></h6>

In addition to the details provided in the "Browse Courses" view, the course detail view additionally provides a user with visibility of the various chapters/ sections in a course. From this view, users are able to either begin or resume courses they've already started

<h6><b> Chapter Page </b></h6>

This is the in course chapter view where users can read the content of a chapter from a course. Additionally, users are also able to navigate to the previous, next or jump to any specific chapter within a course

<h6><b> Feedback Page </b></h6>

Once a user completes a course, they are presented with a feedback page. Here, the user can provide a rating (on a scale from 1 - 5) indicating whether they enjoyed the course, as well as provide text feedback to the site administrators. 

