---
id: api_documentation
title: API Documentation
sidebar_label: API Documentation
---

Base URLs:

* <a href="http://localhost:8081/">http://localhost:8081/</a>

## **Admin API**

<!-- Category Api Controller -->

### Fetch Lms Category

<a id="opIdgetLmsCategory"></a>

<p>This api is used to fetch all categories.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/admin/category");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8081/admin/category HTTP/1.1
Host: localhost:8081
Accept: */*

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'*/*'
};

fetch('http://localhost:8081/admin/category',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /admin/category`



<h3 id="fetch-lms-category-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[CategoryResponse](#schemacategoryresponse)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Create Lms Category

<a id="opIdcreateLmsCategory"></a>
<p>This api is used to Create categories.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/admin/category");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8081/admin/category HTTP/1.1
Host: localhost:8081
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "color": "string",
  "description": "string",
  "file_type": "string",
  "logo": "string",
  "name": "string",
  "published": true
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8081/admin/category',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /admin/category`

> Body parameter

```json
{
  "color": "string",
  "description": "string",
  "file_type": "string",
  "logo": "string",
  "name": "string",
  "published": true
}
```

<h3 id="create-lms-category-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[LmsCategoryRequest](#schemalmscategoryrequest)|true|lmsCategoryRequest|


<h3 id="create-lms-category-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[CategoryResponse](#schemacategoryresponse)|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|successful operation|[CategoryResponse](#schemacategoryresponse)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|string|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Get Lms Category by id

<a id="opIdgetLmsCategoryById"></a>
<p>This api is used to get categorie by category id.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/admin/category/{Id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8081/admin/category/{Id} HTTP/1.1
Host: localhost:8081
Accept: */*

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'*/*'
};

fetch('http://localhost:8081/admin/category/{Id}',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /admin/category/{Id}`

<h3 id="get-lms-category-by-id-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|Id|path|integer(int32)|true|Category ID|


<h3 id="get-lms-category-by-id-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[CategoryResponse](#schemacategoryresponse)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Update Lms Category

<a id="opIdupdateLmsCategory"></a>

<p>This api is used to update categorie by id.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/admin/category/{Id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PUT");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PUT http://localhost:8081/admin/category/{Id} HTTP/1.1
Host: localhost:8081
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "color": "string",
  "description": "string",
  "file_type": "string",
  "logo": "string",
  "name": "string",
  "published": true
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8081/admin/category/{Id}',
{
  method: 'PUT',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`PUT /admin/category/{Id}`

> Body parameter

```json
{
  "color": "string",
  "description": "string",
  "file_type": "string",
  "logo": "string",
  "name": "string",
  "published": true
}
```

<h3 id="update-lms-category-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|Id|path|integer(int32)|true|Category ID|
|body|body|[LmsCategoryRequest](#schemalmscategoryrequest)|true|lmsCategoryRequest|


<h3 id="update-lms-category-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[CategoryResponse](#schemacategoryresponse)|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Publish or UnPublish Lms Category

<a id="opIdpublishUnPublishCategory"></a>
<p>This api is used to publish or unpublish categorie by category id. only published category will be shown to users.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/admin/category/{Id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PATCH");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PATCH http://localhost:8081/admin/category/{Id} HTTP/1.1
Host: localhost:8081
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "published": true
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8081/admin/category/{Id}',
{
  method: 'PATCH',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`PATCH /admin/category/{Id}`

> Body parameter

```json
{
  "published": true
}
```

<h3 id="publish-or-unpublish-lms-category-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|Id|path|integer(int32)|true|Category ID|
|body|body|[PublishRequest](#schemapublishrequest)|true|publishRequest|


<h3 id="publish-or-unpublish-lms-category-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[CategoryResponse](#schemacategoryresponse)|
|204|[No Content](https://tools.ietf.org/html/rfc7231#section-6.3.5)|successful operation|string|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Fetch Lms Course

<a id="opIdgetLmsCourse"></a>
<p>This api is used to fetch all course.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/admin/course");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8081/admin/course HTTP/1.1
Host: localhost:8081
Accept: */*

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'*/*'
};

fetch('http://localhost:8081/admin/course',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /admin/course`


<h3 id="fetch-lms-course-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[CourseResponse](#schemacourseresponse)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Create Lms Course

<a id="opIdcreateLmsCourse"></a>
<p>This api is used to create course. Every course is linked with category created in above step.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/admin/course");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8081/admin/course HTTP/1.1
Host: localhost:8081
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "category_id": 0,
  "chapters": [
    {
      "chronology": 0,
      "content": "string",
      "id": 0,
      "name": "string"
    }
  ],
  "deleted_chapters": [
    0
  ],
  "file": "string",
  "file_type": "string",
  "id": 0,
  "name": "string",
  "overview": "string",
  "published": true
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8081/admin/course',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /admin/course`

> Body parameter

```json
{
  "category_id": 0,
  "chapters": [
    {
      "chronology": 0,
      "content": "string",
      "id": 0,
      "name": "string"
    }
  ],
  "deleted_chapters": [
    0
  ],
  "file": "string",
  "file_type": "string",
  "id": 0,
  "name": "string",
  "overview": "string",
  "published": true
}
```

<h3 id="create-lms-course-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[CourseRequest](#schemacourserequest)|true|courseRequest|


<h3 id="create-lms-course-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[CourseResponse](#schemacourseresponse)|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|successful operation|[CourseResponse](#schemacourseresponse)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Get Course By Id

<a id="opIdpublishUnPublishCourse"></a>
<p>This api is used to get course by course id.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/admin/course/{Id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8081/admin/course/{Id} HTTP/1.1
Host: localhost:8081
Accept: */*

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'*/*'
};

fetch('http://localhost:8081/admin/course/{Id}',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /admin/course/{Id}`

<h3 id="get-course-by-id-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|Id|path|integer(int32)|true|Course ID|


<h3 id="get-course-by-id-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[CourseResponse](#schemacourseresponse)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Update Lms Course

<a id="opIdcreateLmsCourse_1"></a>
<p>This api is used to update course by course id.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/admin/course/{Id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PUT");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PUT http://localhost:8081/admin/course/{Id} HTTP/1.1
Host: localhost:8081
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "category_id": 0,
  "chapters": [
    {
      "chronology": 0,
      "content": "string",
      "id": 0,
      "name": "string"
    }
  ],
  "deleted_chapters": [
    0
  ],
  "file": "string",
  "file_type": "string",
  "id": 0,
  "name": "string",
  "overview": "string",
  "published": true
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8081/admin/course/{Id}',
{
  method: 'PUT',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`PUT /admin/course/{Id}`

> Body parameter

```json
{
  "category_id": 0,
  "chapters": [
    {
      "chronology": 0,
      "content": "string",
      "id": 0,
      "name": "string"
    }
  ],
  "deleted_chapters": [
    0
  ],
  "file": "string",
  "file_type": "string",
  "id": 0,
  "name": "string",
  "overview": "string",
  "published": true
}
```

<h3 id="update-lms-course-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|Id|path|integer(int32)|true|Course ID|
|body|body|[CourseRequest](#schemacourserequest)|true|courseRequest|


<h3 id="update-lms-course-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[CourseResponse](#schemacourseresponse)|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Publish or UnPublish Lms CourseRequest

<a id="opIdpublishUnPublishCourse_1"></a>
<p>This api is used to publish or unpublish course by course id.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/admin/course/{Id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PATCH");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PATCH http://localhost:8081/admin/course/{Id} HTTP/1.1
Host: localhost:8081
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "published": true
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8081/admin/course/{Id}',
{
  method: 'PATCH',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`PATCH /admin/course/{Id}`

> Body parameter

```json
{
  "published": true
}
```

<h3 id="publish-or-unpublish-lms-courserequest-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|Id|path|integer(int32)|true|Course ID|
|body|body|[PublishRequest](#schemapublishrequest)|true|publishRequest|


<h3 id="publish-or-unpublish-lms-courserequest-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|OK|[CategoryResponse](#schemacategoryresponse)|
|204|[No Content](https://tools.ietf.org/html/rfc7231#section-6.3.5)|successful operation|string|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

## **File API**

<!-- File Api Controller -->

### Upload Lms Files

<a id="opIduploadLmsFile_1"></a>
<p>This api is used to upload a file.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/file/upload");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8081/file/upload HTTP/1.1
Host: localhost:8081
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "file": "string",
  "file_type": "string"
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8081/file/upload',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /file/upload`

> Body parameter

```json
{
  "file": "string",
  "file_type": "string"
}
```

<h3 id="upload-lms-files-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[FileUploadRequest](#schemafileuploadrequest)|true|fileUploadRequest|


<h3 id="upload-lms-files-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Get Lms Files

<a id="opIduploadLmsFile"></a>
<p>This api is used to get file by file id.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/file/{file_id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8081/file/{file_id} HTTP/1.1
Host: localhost:8081
Accept: application/octet-stream

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/octet-stream'
};

fetch('http://localhost:8081/file/{file_id}',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /file/{file_id}`

<h3 id="get-lms-files-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|file_id|path|string(uuid)|true|File ID|


<h3 id="get-lms-files-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[File](#schemafile)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

## **JobSeeker API**

<!-- Jobseeker Api Controller -->

### Get Enrolled Course (Your Course)

<a id="opIdgetEnrolledCourse_1"></a>
<p>This api is used to get all enrolled course by user.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/jobseekers/{jobseeker_id}/course");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8081/jobseekers/{jobseeker_id}/course HTTP/1.1
Host: localhost:8081
Accept: */*

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'*/*'
};

fetch('http://localhost:8081/jobseekers/{jobseeker_id}/course',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /jobseekers/{jobseeker_id}/course`

<h3 id="get-enrolled-course-(your-course)-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|jobseeker_id|path|integer(int32)|true|Jobseeker's ID|


<h3 id="get-enrolled-course-(your-course)-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|string|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-enrolled-course-(your-course)-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[EnrollCourseResponse](#schemaenrollcourseresponse)]|false|none|none|
|» EnrollCourseResponse|[EnrollCourseResponse](#schemaenrollcourseresponse)|false|none|none|
|»» complete_percentage|integer(int32)|false|none|none|
|»» course_id|integer(int32)|false|none|none|
|»» course_image|string|false|none|none|
|»» course_name|string|false|none|none|
|»» current_chapter_chronology|integer(int32)|false|none|none|
|»» enrollment_date|string(date-time)|false|none|none|
|»» enrollment_id|integer(int32)|false|none|none|
|»» last_updated_date|string(date-time)|false|none|none|
|»» max_completed_chapter_chronology|integer(int32)|false|none|none|
|»» total_chapter|integer(int32)|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Check if jobseeker has already enrolled this course 

<a id="opIdgetEnrolledCourse"></a>
<p>This api is used to check if user has enrolled a course or not.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/jobseekers/{jobseeker_id}/course/{course_id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8081/jobseekers/{jobseeker_id}/course/{course_id} HTTP/1.1
Host: localhost:8081
Accept: */*

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'*/*'
};

fetch('http://localhost:8081/jobseekers/{jobseeker_id}/course/{course_id}',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /jobseekers/{jobseeker_id}/course/{course_id}`

<h3 id="check-if-jobseeker-has-already-enrolled-this-course--parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|course_id|path|integer(int32)|true|Course's ID|
|jobseeker_id|path|integer(int32)|true|Jobseeker's ID|


<h3 id="check-if-jobseeker-has-already-enrolled-this-course--responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|string|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="check-if-jobseeker-has-already-enrolled-this-course--responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[JobseekerEnrollmentStatus](#schemajobseekerenrollmentstatus)]|false|none|none|
|» JobseekerEnrollmentStatus|[JobseekerEnrollmentStatus](#schemajobseekerenrollmentstatus)|false|none|none|
|»» enrolled|boolean|false|none|none|
|»» enrollment_id|integer(int32)|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Enroll Course

<a id="opIdenrollCourse"></a>
<p>This api is used to enroll a course to user.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/jobseekers/{jobseeker_id}/course/{course_id}/action/enroll");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8081/jobseekers/{jobseeker_id}/course/{course_id}/action/enroll HTTP/1.1
Host: localhost:8081
Accept: */*

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'*/*'
};

fetch('http://localhost:8081/jobseekers/{jobseeker_id}/course/{course_id}/action/enroll',
{
  method: 'POST',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /jobseekers/{jobseeker_id}/course/{course_id}/action/enroll`

<h3 id="enroll-course-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|course_id|path|integer(int32)|true|Course's ID|
|jobseeker_id|path|integer(int32)|true|Jobseeker's ID|


<h3 id="enroll-course-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[EnrollCourseResponse](#schemaenrollcourseresponse)|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|string|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Update Chapter Status

<a id="opIdupsertCourseChapterStatus"></a>
<p>This api is used to update chapter status for enrollment, so user can continue from where they have left instead from start.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/jobseekers/{jobseeker_id}/enrollment/{enrollment_id}/action/update-chapter-status");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PATCH");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PATCH http://localhost:8081/jobseekers/{jobseeker_id}/enrollment/{enrollment_id}/action/update-chapter-status HTTP/1.1
Host: localhost:8081
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "completed_chapter": 0,
  "current_chapter": 0
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8081/jobseekers/{jobseeker_id}/enrollment/{enrollment_id}/action/update-chapter-status',
{
  method: 'PATCH',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`PATCH /jobseekers/{jobseeker_id}/enrollment/{enrollment_id}/action/update-chapter-status`

> Body parameter

```json
{
  "completed_chapter": 0,
  "current_chapter": 0
}
```

<h3 id="update-chapter-status-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|enrollment_id|path|integer(int32)|true|Enrollment's ID|
|jobseeker_id|path|integer(int32)|true|Jobseeker's ID|
|body|body|[UpdateCourseStatusRequest](#schemaupdatecoursestatusrequest)|true|updateCourseStatusRequest|


<h3 id="update-chapter-status-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|204|[No Content](https://tools.ietf.org/html/rfc7231#section-6.3.5)|No Content|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|string|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Fetch Chapter Status

<a id="opIdfetchCourseChapterStatus"></a>
<p>This api is used to get chapter status for enrollment, so user can continue from where they have left instead from start.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/jobseekers/{jobseeker_id}/enrollment/{enrollment_id}/status");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8081/jobseekers/{jobseeker_id}/enrollment/{enrollment_id}/status HTTP/1.1
Host: localhost:8081
Accept: */*

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'*/*'
};

fetch('http://localhost:8081/jobseekers/{jobseeker_id}/enrollment/{enrollment_id}/status',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /jobseekers/{jobseeker_id}/enrollment/{enrollment_id}/status`

<h3 id="fetch-chapter-status-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|enrollment_id|path|integer(int32)|true|Enrollment's ID|
|jobseeker_id|path|integer(int32)|true|Jobseeker's ID|


<h3 id="fetch-chapter-status-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[CourseStatusResponse](#schemacoursestatusresponse)|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|None|

<aside class="success">
This operation does not require authentication
</aside>

## **Public API**

<!-- Public Api Controller -->

### Fetch Lms Category For Jobseeker

<a id="opIdgetLmsCategoryForJobseeker"></a>
<p>This api is used to get all published categorie for user.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/public/category");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8081/public/category HTTP/1.1
Host: localhost:8081
Accept: */*

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'*/*'
};

fetch('http://localhost:8081/public/category',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /public/category`


<h3 id="fetch-lms-category-for-jobseeker-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[CategoryResponse](#schemacategoryresponse)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Fetch Lms Course For Jobseeker (Browse Course)

<a id="opIdgetLmsCategoryForJobseeker_1"></a>
<p>This api is used to get fetch all published courses.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/public/course");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8081/public/course HTTP/1.1
Host: localhost:8081
Accept: */*

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'*/*'
};

fetch('http://localhost:8081/public/course',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /public/course`

<h3 id="fetch-lms-course-for-jobseeker-(browse-course)-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|category_id|query|integer(int32)|false|category_id|


<h3 id="fetch-lms-course-for-jobseeker-(browse-course)-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[CourseOverview](#schemacourseoverview)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Get Lms Course For By id

<a id="opIdgetLmsCourseByIdForJobseeker"></a>
<p>This api is used to get course by course id. This api will return chapters as well</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/public/course/{id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8081/public/course/{id} HTTP/1.1
Host: localhost:8081
Accept: */*

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'*/*'
};

fetch('http://localhost:8081/public/course/{id}',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /public/course/{id}`

<h3 id="get-lms-course-for-by-id-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int32)|true|Course ID|


<h3 id="get-lms-course-for-by-id-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[CourseOverview](#schemacourseoverview)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Review Course

<a id="opIdreviewCourse"></a>
<p>This api is used to review a cource once user has completed that course.</p>

> Code samples
<!--DOCUSAURUS_CODE_TABS-->
<!--Java-->
```java
URL obj = new URL("http://localhost:8081/public/course/{id}/action/review");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8081/public/course/{id}/action/review HTTP/1.1
Host: localhost:8081
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "email": "string",
  "rating": 0,
  "review": "string"
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8081/public/course/{id}/action/review',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /public/course/{id}/action/review`

> Body parameter

```json
{
  "email": "string",
  "rating": 0,
  "review": "string"
}
```

<h3 id="review-course-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int32)|true|Course ID|
|body|body|[CourseReviewRequest](#schemacoursereviewrequest)|true|courseReviewRequest|


<h3 id="review-course-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[CourseResponse](#schemacourseresponse)|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|string|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>


## **Schemas**

<h2 id="tocS_CategoryResponse">CategoryResponse</h2>

<a id="schemacategoryresponse"></a>
<a id="schema_CategoryResponse"></a>
<a id="tocScategoryresponse"></a>
<a id="tocscategoryresponse"></a>

```json
{
  "color": "string",
  "created_date": "2019-08-24T14:15:22Z",
  "description": "string",
  "id": 0,
  "logo_path": "string",
  "name": "string",
  "published": true,
  "published_date": "2019-08-24T14:15:22Z"
}

```

### Category Response Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|color|string|false|none|none|
|created_date|string(date-time)|false|none|none|
|description|string|false|none|none|
|id|integer(int32)|false|none|none|
|logo_path|string|false|none|none|
|name|string|false|none|none|
|published|boolean|false|none|none|
|published_date|string(date-time)|false|none|none|

<h2 id="tocS_ChapterOverview">ChapterOverview</h2>

<a id="schemachapteroverview"></a>
<a id="schema_ChapterOverview"></a>
<a id="tocSchapteroverview"></a>
<a id="tocschapteroverview"></a>

```json
{
  "chronology": 0,
  "content": "string",
  "name": "string"
}

```



### Chapter Overview Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|chronology|integer(int32)|false|none|none|
|content|string|false|none|none|
|name|string|false|none|none|

<h2 id="tocS_CourseChapterRequest">CourseChapterRequest</h2>

<a id="schemacoursechapterrequest"></a>
<a id="schema_CourseChapterRequest"></a>
<a id="tocScoursechapterrequest"></a>
<a id="tocscoursechapterrequest"></a>

```json
{
  "chronology": 0,
  "content": "string",
  "id": 0,
  "name": "string"
}

```

### Course Chapter Request Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|chronology|integer(int32)|false|none|none|
|content|string|false|none|none|
|id|integer(int32)|false|none|none|
|name|string|false|none|none|

<h2 id="tocS_CourseChapterResponse">CourseChapterResponse</h2>

<a id="schemacoursechapterresponse"></a>
<a id="schema_CourseChapterResponse"></a>
<a id="tocScoursechapterresponse"></a>
<a id="tocscoursechapterresponse"></a>

```json
{
  "chronology": 0,
  "content": "string",
  "id": 0,
  "name": "string"
}

```


### Course Chapter Response Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|chronology|integer(int32)|false|none|none|
|content|string|false|none|none|
|id|integer(int32)|false|none|none|
|name|string|false|none|none|

<h2 id="tocS_CourseOverview">CourseOverview</h2>

<a id="schemacourseoverview"></a>
<a id="schema_CourseOverview"></a>
<a id="tocScourseoverview"></a>
<a id="tocscourseoverview"></a>

```json
{
  "category_color": "string",
  "category_id": 0,
  "category_name": "string",
  "chapters": [
    {
      "chronology": 0,
      "content": "string",
      "name": "string"
    }
  ],
  "file_url": "string",
  "id": 0,
  "likes": 0,
  "name": "string",
  "overview": "string",
  "publishedDate": {
    "date": 0,
    "day": 0,
    "hours": 0,
    "minutes": 0,
    "month": 0,
    "nanos": 0,
    "seconds": 0,
    "time": 0,
    "timezoneOffset": 0,
    "year": 0
  },
  "timeToRead": "string"
}

```


### Course Overview Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|category_color|string|false|none|none|
|category_id|integer(int32)|false|none|none|
|category_name|string|false|none|none|
|chapters|[[ChapterOverview](#schemachapteroverview)]|false|none|none|
|file_url|string|false|none|none|
|id|integer(int32)|false|none|none|
|likes|integer(int32)|false|none|none|
|name|string|false|none|none|
|overview|string|false|none|none|
|publishedDate|[Timestamp](#schematimestamp)|false|none|none|
|timeToRead|string|false|none|none|

<h2 id="tocS_CourseRequest">CourseRequest</h2>

<a id="schemacourserequest"></a>
<a id="schema_CourseRequest"></a>
<a id="tocScourserequest"></a>
<a id="tocscourserequest"></a>

```json
{
  "category_id": 0,
  "chapters": [
    {
      "chronology": 0,
      "content": "string",
      "id": 0,
      "name": "string"
    }
  ],
  "deleted_chapters": [
    0
  ],
  "file": "string",
  "file_type": "string",
  "id": 0,
  "name": "string",
  "overview": "string",
  "published": true
}

```


### Course Request Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|category_id|integer(int32)|false|none|none|
|chapters|[[CourseChapterRequest](#schemacoursechapterrequest)]|false|none|none|
|deleted_chapters|[integer]|false|none|none|
|file|string(byte)|false|none|none|
|file_type|string|false|none|none|
|id|integer(int32)|false|none|none|
|name|string|false|none|none|
|overview|string|false|none|none|
|published|boolean|false|none|none|

<h2 id="tocS_CourseResponse">CourseResponse</h2>

<a id="schemacourseresponse"></a>
<a id="schema_CourseResponse"></a>
<a id="tocScourseresponse"></a>
<a id="tocscourseresponse"></a>

```json
{
  "category_id": 0,
  "category_name": "string",
  "chapters": [
    {
      "chronology": 0,
      "content": "string",
      "id": 0,
      "name": "string"
    }
  ],
  "created_date": "2019-08-24T14:15:22Z",
  "file_url": "string",
  "id": 0,
  "name": "string",
  "overview": "string",
  "published": true,
  "published_date": "2019-08-24T14:15:22Z",
  "time_to_read": 0
}

```


### Course Response Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|category_id|integer(int32)|false|none|none|
|category_name|string|false|none|none|
|chapters|[[CourseChapterResponse](#schemacoursechapterresponse)]|false|none|none|
|created_date|string(date-time)|false|none|none|
|file_url|string|false|none|none|
|id|integer(int32)|false|none|none|
|name|string|false|none|none|
|overview|string|false|none|none|
|published|boolean|false|none|none|
|published_date|string(date-time)|false|none|none|
|time_to_read|integer(int32)|false|none|none|

<h2 id="tocS_CourseReviewRequest">CourseReviewRequest</h2>

<a id="schemacoursereviewrequest"></a>
<a id="schema_CourseReviewRequest"></a>
<a id="tocScoursereviewrequest"></a>
<a id="tocscoursereviewrequest"></a>

```json
{
  "email": "string",
  "rating": 0,
  "review": "string"
}

```


### Course Review Request Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|email|string|false|none|none|
|rating|integer(int32)|false|none|none|
|review|string|false|none|none|

<h2 id="tocS_CourseStatusResponse">CourseStatusResponse</h2>

<a id="schemacoursestatusresponse"></a>
<a id="schema_CourseStatusResponse"></a>
<a id="tocScoursestatusresponse"></a>
<a id="tocscoursestatusresponse"></a>

```json
{
  "chapters": [
    {
      "chronology": 0,
      "content": "string",
      "id": 0,
      "name": "string"
    }
  ],
  "complete_percentage": 0,
  "course_id": 0,
  "course_image": "string",
  "course_name": "string",
  "current_chapter_chronology": 0,
  "enrollment_date": "2019-08-24T14:15:22Z",
  "enrollment_id": 0,
  "last_updated_date": "2019-08-24T14:15:22Z",
  "max_completed_chapter_chronology": 0,
  "total_chapter": 0
}

```

### Course Status Response Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|chapters|[[CourseChapterResponse](#schemacoursechapterresponse)]|false|none|none|
|complete_percentage|integer(int32)|false|none|none|
|course_id|integer(int32)|false|none|none|
|course_image|string|false|none|none|
|course_name|string|false|none|none|
|current_chapter_chronology|integer(int32)|false|none|none|
|enrollment_date|string(date-time)|false|none|none|
|enrollment_id|integer(int32)|false|none|none|
|last_updated_date|string(date-time)|false|none|none|
|max_completed_chapter_chronology|integer(int32)|false|none|none|
|total_chapter|integer(int32)|false|none|none|

<h2 id="tocS_EnrollCourseResponse">EnrollCourseResponse</h2>

<a id="schemaenrollcourseresponse"></a>
<a id="schema_EnrollCourseResponse"></a>
<a id="tocSenrollcourseresponse"></a>
<a id="tocsenrollcourseresponse"></a>

```json
{
  "complete_percentage": 0,
  "course_id": 0,
  "course_image": "string",
  "course_name": "string",
  "current_chapter_chronology": 0,
  "enrollment_date": "2019-08-24T14:15:22Z",
  "enrollment_id": 0,
  "last_updated_date": "2019-08-24T14:15:22Z",
  "max_completed_chapter_chronology": 0,
  "total_chapter": 0
}

```



### Enroll Course Response Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|complete_percentage|integer(int32)|false|none|none|
|course_id|integer(int32)|false|none|none|
|course_image|string|false|none|none|
|course_name|string|false|none|none|
|current_chapter_chronology|integer(int32)|false|none|none|
|enrollment_date|string(date-time)|false|none|none|
|enrollment_id|integer(int32)|false|none|none|
|last_updated_date|string(date-time)|false|none|none|
|max_completed_chapter_chronology|integer(int32)|false|none|none|
|total_chapter|integer(int32)|false|none|none|

<h2 id="tocS_File">File</h2>

<a id="schemafile"></a>
<a id="schema_File"></a>
<a id="tocSfile"></a>
<a id="tocsfile"></a>

```json
{
  "absolute": true,
  "absoluteFile": {
    "absolute": true,
    "absoluteFile": {
      "absolute": true,
      "absoluteFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "absolutePath": "string",
      "canonicalFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "canonicalPath": "string",
      "directory": true,
      "file": true,
      "freeSpace": 0,
      "hidden": true,
      "name": "string",
      "parent": "string",
      "parentFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "path": "string",
      "totalSpace": 0,
      "usableSpace": 0
    },
    "absolutePath": "string",
    "canonicalFile": {
      "absolute": true,
      "absoluteFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "absolutePath": "string",
      "canonicalFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "canonicalPath": "string",
      "directory": true,
      "file": true,
      "freeSpace": 0,
      "hidden": true,
      "name": "string",
      "parent": "string",
      "parentFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "path": "string",
      "totalSpace": 0,
      "usableSpace": 0
    },
    "canonicalPath": "string",
    "directory": true,
    "file": true,
    "freeSpace": 0,
    "hidden": true,
    "name": "string",
    "parent": "string",
    "parentFile": {
      "absolute": true,
      "absoluteFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "absolutePath": "string",
      "canonicalFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "canonicalPath": "string",
      "directory": true,
      "file": true,
      "freeSpace": 0,
      "hidden": true,
      "name": "string",
      "parent": "string",
      "parentFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "path": "string",
      "totalSpace": 0,
      "usableSpace": 0
    },
    "path": "string",
    "totalSpace": 0,
    "usableSpace": 0
  },
  "absolutePath": "string",
  "canonicalFile": {
    "absolute": true,
    "absoluteFile": {
      "absolute": true,
      "absoluteFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "absolutePath": "string",
      "canonicalFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "canonicalPath": "string",
      "directory": true,
      "file": true,
      "freeSpace": 0,
      "hidden": true,
      "name": "string",
      "parent": "string",
      "parentFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "path": "string",
      "totalSpace": 0,
      "usableSpace": 0
    },
    "absolutePath": "string",
    "canonicalFile": {
      "absolute": true,
      "absoluteFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "absolutePath": "string",
      "canonicalFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "canonicalPath": "string",
      "directory": true,
      "file": true,
      "freeSpace": 0,
      "hidden": true,
      "name": "string",
      "parent": "string",
      "parentFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "path": "string",
      "totalSpace": 0,
      "usableSpace": 0
    },
    "canonicalPath": "string",
    "directory": true,
    "file": true,
    "freeSpace": 0,
    "hidden": true,
    "name": "string",
    "parent": "string",
    "parentFile": {
      "absolute": true,
      "absoluteFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "absolutePath": "string",
      "canonicalFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "canonicalPath": "string",
      "directory": true,
      "file": true,
      "freeSpace": 0,
      "hidden": true,
      "name": "string",
      "parent": "string",
      "parentFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "path": "string",
      "totalSpace": 0,
      "usableSpace": 0
    },
    "path": "string",
    "totalSpace": 0,
    "usableSpace": 0
  },
  "canonicalPath": "string",
  "directory": true,
  "file": true,
  "freeSpace": 0,
  "hidden": true,
  "name": "string",
  "parent": "string",
  "parentFile": {
    "absolute": true,
    "absoluteFile": {
      "absolute": true,
      "absoluteFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "absolutePath": "string",
      "canonicalFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "canonicalPath": "string",
      "directory": true,
      "file": true,
      "freeSpace": 0,
      "hidden": true,
      "name": "string",
      "parent": "string",
      "parentFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "path": "string",
      "totalSpace": 0,
      "usableSpace": 0
    },
    "absolutePath": "string",
    "canonicalFile": {
      "absolute": true,
      "absoluteFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "absolutePath": "string",
      "canonicalFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "canonicalPath": "string",
      "directory": true,
      "file": true,
      "freeSpace": 0,
      "hidden": true,
      "name": "string",
      "parent": "string",
      "parentFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "path": "string",
      "totalSpace": 0,
      "usableSpace": 0
    },
    "canonicalPath": "string",
    "directory": true,
    "file": true,
    "freeSpace": 0,
    "hidden": true,
    "name": "string",
    "parent": "string",
    "parentFile": {
      "absolute": true,
      "absoluteFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "absolutePath": "string",
      "canonicalFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "canonicalPath": "string",
      "directory": true,
      "file": true,
      "freeSpace": 0,
      "hidden": true,
      "name": "string",
      "parent": "string",
      "parentFile": {
        "absolute": true,
        "absoluteFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "absolutePath": "string",
        "canonicalFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "canonicalPath": "string",
        "directory": true,
        "file": true,
        "freeSpace": 0,
        "hidden": true,
        "name": "string",
        "parent": "string",
        "parentFile": {
          "absolute": null,
          "absoluteFile": null,
          "absolutePath": null,
          "canonicalFile": null,
          "canonicalPath": null,
          "directory": null,
          "file": null,
          "freeSpace": null,
          "hidden": null,
          "name": null,
          "parent": null,
          "parentFile": null,
          "path": null,
          "totalSpace": null,
          "usableSpace": null
        },
        "path": "string",
        "totalSpace": 0,
        "usableSpace": 0
      },
      "path": "string",
      "totalSpace": 0,
      "usableSpace": 0
    },
    "path": "string",
    "totalSpace": 0,
    "usableSpace": 0
  },
  "path": "string",
  "totalSpace": 0,
  "usableSpace": 0
}

```


### File Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|absolute|boolean|false|none|none|
|absoluteFile|[File](#schemafile)|false|none|none|
|absolutePath|string|false|none|none|
|canonicalFile|[File](#schemafile)|false|none|none|
|canonicalPath|string|false|none|none|
|directory|boolean|false|none|none|
|file|boolean|false|none|none|
|freeSpace|integer(int64)|false|none|none|
|hidden|boolean|false|none|none|
|name|string|false|none|none|
|parent|string|false|none|none|
|parentFile|[File](#schemafile)|false|none|none|
|path|string|false|none|none|
|totalSpace|integer(int64)|false|none|none|
|usableSpace|integer(int64)|false|none|none|

<h2 id="tocS_FileUploadRequest">FileUploadRequest</h2>

<a id="schemafileuploadrequest"></a>
<a id="schema_FileUploadRequest"></a>
<a id="tocSfileuploadrequest"></a>
<a id="tocsfileuploadrequest"></a>

```json
{
  "file": "string",
  "file_type": "string"
}

```

### File Upload Request Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|file|string(byte)|false|none|none|
|file_type|string|false|none|none|

<h2 id="tocS_JobseekerEnrollmentStatus">JobseekerEnrollmentStatus</h2>

<a id="schemajobseekerenrollmentstatus"></a>
<a id="schema_JobseekerEnrollmentStatus"></a>
<a id="tocSjobseekerenrollmentstatus"></a>
<a id="tocsjobseekerenrollmentstatus"></a>

```json
{
  "enrolled": true,
  "enrollment_id": 0
}

```


### Jobseeker Enrollment Status Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|enrolled|boolean|false|none|none|
|enrollment_id|integer(int32)|false|none|none|

<h2 id="tocS_LmsCategoryRequest">LmsCategoryRequest</h2>

<a id="schemalmscategoryrequest"></a>
<a id="schema_LmsCategoryRequest"></a>
<a id="tocSlmscategoryrequest"></a>
<a id="tocslmscategoryrequest"></a>

```json
{
  "color": "string",
  "description": "string",
  "file_type": "string",
  "logo": "string",
  "name": "string",
  "published": true
}

```


### Lms Category Request Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|color|string|false|none|none|
|description|string|false|none|none|
|file_type|string|false|none|none|
|logo|string(byte)|false|none|none|
|name|string|false|none|none|
|published|boolean|false|none|none|

<h2 id="tocS_PublishRequest">PublishRequest</h2>

<a id="schemapublishrequest"></a>
<a id="schema_PublishRequest"></a>
<a id="tocSpublishrequest"></a>
<a id="tocspublishrequest"></a>

```json
{
  "published": true
}

```


### Publish Request Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|published|boolean|false|none|none|

<h2 id="tocS_ResponseEntity">ResponseEntity</h2>

<a id="schemaresponseentity"></a>
<a id="schema_ResponseEntity"></a>
<a id="tocSresponseentity"></a>
<a id="tocsresponseentity"></a>

```json
{
  "body": {},
  "statusCode": "100 CONTINUE",
  "statusCodeValue": 0
}

```


### Response Entity Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|body|object|false|none|none|
|statusCode|string|false|none|none|
|statusCodeValue|integer(int32)|false|none|none|

#### Enumerated Values

|Property|Value|
|---|---|
|statusCode|100 CONTINUE|
|statusCode|101 SWITCHING_PROTOCOLS|
|statusCode|102 PROCESSING|
|statusCode|103 CHECKPOINT|
|statusCode|200 OK|
|statusCode|201 CREATED|
|statusCode|202 ACCEPTED|
|statusCode|203 NON_AUTHORITATIVE_INFORMATION|
|statusCode|204 NO_CONTENT|
|statusCode|205 RESET_CONTENT|
|statusCode|206 PARTIAL_CONTENT|
|statusCode|207 MULTI_STATUS|
|statusCode|208 ALREADY_REPORTED|
|statusCode|226 IM_USED|
|statusCode|300 MULTIPLE_CHOICES|
|statusCode|301 MOVED_PERMANENTLY|
|statusCode|302 FOUND|
|statusCode|302 MOVED_TEMPORARILY|
|statusCode|303 SEE_OTHER|
|statusCode|304 NOT_MODIFIED|
|statusCode|305 USE_PROXY|
|statusCode|307 TEMPORARY_REDIRECT|
|statusCode|308 PERMANENT_REDIRECT|
|statusCode|400 BAD_REQUEST|
|statusCode|401 UNAUTHORIZED|
|statusCode|402 PAYMENT_REQUIRED|
|statusCode|403 FORBIDDEN|
|statusCode|404 NOT_FOUND|
|statusCode|405 METHOD_NOT_ALLOWED|
|statusCode|406 NOT_ACCEPTABLE|
|statusCode|407 PROXY_AUTHENTICATION_REQUIRED|
|statusCode|408 REQUEST_TIMEOUT|
|statusCode|409 CONFLICT|
|statusCode|410 GONE|
|statusCode|411 LENGTH_REQUIRED|
|statusCode|412 PRECONDITION_FAILED|
|statusCode|413 PAYLOAD_TOO_LARGE|
|statusCode|413 REQUEST_ENTITY_TOO_LARGE|
|statusCode|414 URI_TOO_LONG|
|statusCode|414 REQUEST_URI_TOO_LONG|
|statusCode|415 UNSUPPORTED_MEDIA_TYPE|
|statusCode|416 REQUESTED_RANGE_NOT_SATISFIABLE|
|statusCode|417 EXPECTATION_FAILED|
|statusCode|418 I_AM_A_TEAPOT|
|statusCode|419 INSUFFICIENT_SPACE_ON_RESOURCE|
|statusCode|420 METHOD_FAILURE|
|statusCode|421 DESTINATION_LOCKED|
|statusCode|422 UNPROCESSABLE_ENTITY|
|statusCode|423 LOCKED|
|statusCode|424 FAILED_DEPENDENCY|
|statusCode|426 UPGRADE_REQUIRED|
|statusCode|428 PRECONDITION_REQUIRED|
|statusCode|429 TOO_MANY_REQUESTS|
|statusCode|431 REQUEST_HEADER_FIELDS_TOO_LARGE|
|statusCode|451 UNAVAILABLE_FOR_LEGAL_REASONS|
|statusCode|500 INTERNAL_SERVER_ERROR|
|statusCode|501 NOT_IMPLEMENTED|
|statusCode|502 BAD_GATEWAY|
|statusCode|503 SERVICE_UNAVAILABLE|
|statusCode|504 GATEWAY_TIMEOUT|
|statusCode|505 HTTP_VERSION_NOT_SUPPORTED|
|statusCode|506 VARIANT_ALSO_NEGOTIATES|
|statusCode|507 INSUFFICIENT_STORAGE|
|statusCode|508 LOOP_DETECTED|
|statusCode|509 BANDWIDTH_LIMIT_EXCEEDED|
|statusCode|510 NOT_EXTENDED|
|statusCode|511 NETWORK_AUTHENTICATION_REQUIRED|

<h2 id="tocS_Timestamp">Timestamp</h2>

<a id="schematimestamp"></a>
<a id="schema_Timestamp"></a>
<a id="tocStimestamp"></a>
<a id="tocstimestamp"></a>

```json
{
  "date": 0,
  "day": 0,
  "hours": 0,
  "minutes": 0,
  "month": 0,
  "nanos": 0,
  "seconds": 0,
  "time": 0,
  "timezoneOffset": 0,
  "year": 0
}

```


### Timestamp Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|date|integer(int32)|false|none|none|
|day|integer(int32)|false|none|none|
|hours|integer(int32)|false|none|none|
|minutes|integer(int32)|false|none|none|
|month|integer(int32)|false|none|none|
|nanos|integer(int32)|false|none|none|
|seconds|integer(int32)|false|none|none|
|time|integer(int64)|false|none|none|
|timezoneOffset|integer(int32)|false|none|none|
|year|integer(int32)|false|none|none|

<h2 id="tocS_UpdateCourseStatusRequest">UpdateCourseStatusRequest</h2>

<a id="schemaupdatecoursestatusrequest"></a>
<a id="schema_UpdateCourseStatusRequest"></a>
<a id="tocSupdatecoursestatusrequest"></a>
<a id="tocsupdatecoursestatusrequest"></a>

```json
{
  "completed_chapter": 0,
  "current_chapter": 0
}

```


### Update Course Status Request Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|completed_chapter|integer(int32)|false|none|none|
|current_chapter|integer(int32)|false|none|none|

