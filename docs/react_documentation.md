---
id: react_documentation
title: React documentation 
sidebar_label: React documentation 
---

 <h3>Home Page</h3>


![Home page](/lms/img/home.png)

* **Description**
    * All data here is static info except for the list of categories which is pulled dynamically
* **APIs Used**
    * [Get categories](api_documentation#fetch-lms-category)
* **Actions**
    1.  Browse all courses - navigate to "Browse Courses" page
    2. Start Now CTA on Category Card - Takes the user to the "browse courses" page with the selected category filter applied



<h3>Browse Courses Page</h3>

![Browse Courses page](/lms/img/browse-courses.png)


* **Description**
    * Browse different courses with filter options by category and sort by options.
* **APIs Used**
    * [Get categories](api_documentation#fetch-lms-category)
    * [Get courses](api_documentation#fetch-lms-course)
* **Actions**
    1. Filter by category - API is recalled based on category
    2. Sort by changes, Sorting happens on frontend.
    3. Expand any course detail
    4. Share course on social media
    5. Enroll to a course - Takes you to course detail page.


<h3>Course Detail Page</h3>

![Browse Courses page](/lms/img/course-detail.png)

* **Description**
    * View Course Detail
* **APIs Used**
    * [Get course detail](api_documentation#get-course-by-id)
* **Actions**
    1. Share course on social media.
    2. Start a course - Takes you to course chapters page


<h3>Course Chapter Page</h3>

![Browse Courses page](/lms/img/course-chapter.png)

* **Description**
    * Go through Course content chapter by chapter
    * chapters details are retrieved via course API and is changed on the frontend based on the current chapter
* **APIs Used**
    * [Get course detail](api_documentation#get-course-by-id)
* **Actions**
    1. Go to next chapter
    2. Go to previous chapter
    3. Go to course overview - Go back to course detail - Skip to a particular chapter
    4. When clicking next on the last chapter, the user is then navigated to the feedback page



<h3>Course Feedback Page</h3>

![Feedback page](/lms/img/feedback.png)


* **Description**
    * Give feedback to a course in between 1 to 5, 5 being the best.
* **APIs Used**
    * [Get course detail](api_documentation#get-course-by-id)
    * [Post feedback](api_documentation#review-course)
* **Actions**
    1. Submit feedback (Rating and email are compulsory, comments are optional)


<h3>Admin - Category list</h3>

![Category List](/lms/img/admin-category-table.png)

* **Description**
    * See list of categories, Navigate to add/edit category and publish/unpublish category
* **APIs Used**
    * [Get categories](api_documentation#fetch-lms-category)

* **Actions**
    1. Publish/unpublish category
    2. Navigate to add/edit category

<h3>Admin - Category Add/Edit</h3>

![Category Form](/lms/img/admin-category-add.png)

* **Description**
    * Create a new category or edit existing category
* **APIs Used**
    * [Create category](api_documentation#create-lms-category)
    * [Update category](api_documentation#update-lms-category)

* **Actions**
    1. Add/Edit category


<h3>Admin - Course list</h3>


![Course List](/lms/img/admin-course-table.png)

* **Description**
    * See list of courses
    * Navigate to add/edit course
    * Publish/ un-publish course
* **APIs Used**
    * [Fetch LMS course](api_documentation#fetch-lms-course)
    * [Publish/Unpublish LMS Course](api_documentation#publish-or-unpublish-lms-courserequest)

* **Actions**
    1. Publish/unpublish course
    2. Navigate to add/edit course 


<h3>Admin - Course Add/Edit</h3>

![Course Form](/lms/img/admin-course-add.png)


* **Description**
    * Create a new course or edit existing course
* **APIs Used**
    * [Create LMS course](api_documentation#create-lms-course)
    * [Edit LMS course](api_documentation#update-lms-course)

* **Actions**
    1. Add/Edit course
