---
id: license_details
title: License details
sidebar_label: License details
---

This project is licensed under the GNU General Public License v3.0 [Learn more](https://choosealicense.com/licenses/gpl-3.0/)